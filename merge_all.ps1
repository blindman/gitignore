$outputFile = "./global.gitignore"
$enabledDir = "./enabled"

if (Test-Path $outputFile) {
  $timestamp = Get-Date -Format FileDateTimeUniversal | ForEach-Object { $_ -replace ":", "." }
  Rename-Item -Path $outputFile -NewName "$outputFile.$timestamp"
}

foreach ($file in Get-ChildItem -Path "./$enabledDir/*.gitignore" -Recurse -Name)
{
  Get-Content -Path "./$enabledDir/$file" | Out-File $outputFile -Append
}
